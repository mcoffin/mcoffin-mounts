use std::{
    env,
    ffi::OsStr,
    sync::Once,
};

fn env_or_default<K, V, F>(key: K, value: F)
where
    K: AsRef<OsStr>,
    V: AsRef<OsStr>,
    F: FnOnce() -> V,
{
    let key = key.as_ref();
    if env::var_os(key).filter(|s| s.len() > 0).is_none() {
        env::set_var(key, value())
    }
}

#[cfg(debug_assertions)]
pub const DEFAULT_LOG_LEVEL: &'static str = "debug";
#[cfg(not(debug_assertions))]
pub const DEFAULT_LOG_LEVEL: &'static str = "info";

static INIT_LOGGING: Once = Once::new();

pub fn init() {
    INIT_LOGGING.call_once(|| {
        env_or_default("RUST_LOG", || format!("{}={}", env!("CARGO_CRATE_NAME"), DEFAULT_LOG_LEVEL));
        env_logger::init();
    });
}
