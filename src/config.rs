use serde::{
    Serialize,
    Deserialize,
};
use std::{
    collections::HashMap,
    error::Error,
    io,
    iter,
    fs,
    fmt,
    path::{
        Path,
        PathBuf,
    },
};

#[derive(Debug, thiserror::Error)]
pub enum ConfigError {
    #[error("Error reading config file: {0}")]
    Io(#[from] io::Error),
    #[error("Error parsing config file: {0}")]
    Parse(#[from] serde_yaml::Error),
}

pub trait ParseConfig: Sized {
    type Error: Error;
    fn parse_config<P: AsRef<Path>>(p: P) -> Result<Self, Self::Error>;
}

impl<T> ParseConfig for T
where
    T: for<'de> Deserialize<'de>,
{
    type Error = ConfigError;

    fn parse_config<P: AsRef<Path>>(p: P) -> Result<Self, Self::Error> {
        let f = fs::OpenOptions::new()
            .read(true)
            .open(p)?;
        serde_yaml::from_reader(f)
            .map_err(ConfigError::from)
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Config {
    mounts: HashMap<String, Mount>,
}

impl Config {
    #[inline(always)]
    pub fn mounts<'a>(&'a self) -> impl Iterator<Item=(&str, &Mount)> {
        self.mounts.iter().map(|(name, v)| (name.as_ref(), v))
    }

    pub fn get_mount<K: AsRef<str>>(&self, key: K) -> Option<&Mount> {
        self.mounts.get(key.as_ref())
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Mount {
    remote: Remote,
    remote_path: PathBuf,
    local_path: PathBuf,
    options: HashMap<String, String>,
}

impl Mount {
    pub fn display<'a>(&'a self) -> MountDisplay<'a> {
        MountDisplay::from(self)
    }

    pub fn uri(&self) -> String {
        format!("//{}/{}", &self.remote.address, self.remote_path.display())
    }

    #[inline(always)]
    pub fn local_path(&self) -> &Path {
        self.local_path.as_ref()
    }

    pub fn mount_options<'a>(&'a self) -> impl Iterator<Item=(&'a str, &'a str)> + 'a {
        self.remote.auth_options()
            .chain(self.options.iter().map(|(k, v)| (k.as_ref(), v.as_ref())))
    }

    pub fn mount_options_string(&self) -> String {
        let mut options = self.mount_options();
        let mut ret = String::from("");
        if let Some((k, v)) = options.next() {
            ret.extend([k, "=", v].iter().map(|&v| v));
        }
        for (k, v) in options {
            ret.extend([",", k, "=", v].iter().map(|&v| v));
        }
        ret
    }
}

#[derive(Debug, Clone, Copy)]
pub struct MountDisplay<'a> {
    address: &'a str,
    remote_path: &'a Path,
    local_path: &'a Path,
}

impl<'a> From<&'a Mount> for MountDisplay<'a> {
    fn from(mount: &'a Mount) -> Self {
        MountDisplay {
            address: mount.remote.address.as_ref(),
            remote_path: mount.remote_path.as_ref(),
            local_path: mount.local_path.as_ref(),
        }
    }
}

impl<'a> fmt::Display for MountDisplay<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} -> //{}/{}", self.local_path.display(), self.address, self.remote_path.display())
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Remote {
    address: String,
    username: String,
    password: String,
}

impl Remote {
    #[inline(always)]
    pub fn username(&self) -> &str {
        self.username.as_ref()
    }

    #[inline(always)]
    pub fn password(&self) -> &str {
        self.password.as_ref()
    }

    fn auth_options<'a>(&'a self) -> impl Iterator<Item=(&'a str, &'a str)> + 'a {
        iter::once(("username", self.username()))
            .chain(iter::once(("password", self.password())))
    }
}
