extern crate clap;
extern crate serde;
extern crate serde_yaml;
#[macro_use] extern crate log;
extern crate env_logger;
extern crate dirs;
extern crate thiserror;
extern crate colored;

mod config;
mod logging;
pub(crate) mod util;

use clap::{
    crate_version,
    crate_description,
    crate_authors,
};
use std::{
    borrow::Cow,
    convert::Infallible,
    error::Error,
    path::{
        Path,
        PathBuf,
    },
    process::{
        self,
        Command,
    },
};
use config::{
    Config,
    ConfigError,
    ParseConfig,
};
use util::{
    CommandError,
};

trait RunnableConfig {
    type Error;
    fn run(&self) -> Result<(), Self::Error>;
}

#[derive(Debug, clap::Parser)]
#[clap(version = crate_version!(), about = crate_description!(), author = crate_authors!())]
struct Options {
    #[clap(long = "config-file", short = 'c', help = "override config file path")]
    config_file_path: Option<PathBuf>,
    #[clap(subcommand)]
    command: SubCommand,
}

#[derive(Debug, clap::Subcommand)]
enum SubCommand {
    #[clap(about = "mount a mount by id from the config file")]
    Mount {
        #[clap(required = true)]
        names: Vec<String>,
    },
    #[clap(alias = "unmount", about = "unmount a mounted mount")]
    Umount {
        #[clap(required = true)]
        names: Vec<String>,
    },
    #[clap(about = "list available mounts from the config file")]
    List {
        #[clap(long, short)]
        verbose: bool
    },
}

#[derive(Debug, thiserror::Error)]
enum MountError {
    #[error("Unknown mount: {0}")]
    UnknownMount(String),
    #[error("Error mounting {0}: {1}")]
    Command(String, CommandError),
}

impl Options {
    fn default_config_file_path() -> Option<PathBuf> {
        use util::ExtMutateMap;
        dirs::config_dir().mutate_map(|p| {
            p.extend(&["mcoffin-mounts", "mounts.yml"])
        })
    }

    pub fn config_file_path<'a>(&'a self) ->  Cow<'a, Path> {
        if let Some(v) = self.config_file_path.as_ref() {
            Cow::Borrowed(v.as_ref())
        } else {
            Cow::Owned(Self::default_config_file_path().unwrap())
        }
    }

    pub fn config(&self) -> Result<Config, ConfigError> {
        Config::parse_config(self.config_file_path())
    }
}

fn do_mount<S: AsRef<str>>(config: &Config, name: S) -> Result<(), MountError> {
    use util::CommandExt;
    let name = name.as_ref();
    let info = config.get_mount(name)
        .map(Ok)
        .unwrap_or_else(|| Err(MountError::UnknownMount(name.to_owned())))?;
    let options = info.mount_options_string();
    debug!("mount options: {}", &options);
    Command::new("sudo")
        .args(&["mount", "-t", "cifs"])
        .arg(&info.uri())
        .arg(info.local_path())
        .args(&["-o", options.as_ref()])
        .status_checked()
        .map_err(|e| MountError::Command(name.to_owned(), e))
        .map(|_| ())
}

fn do_all<It, F, T, E>(it: It, do_f: F, fail_fast: bool) -> Result<(), E>
where
    It: IntoIterator<Item=T>,
    F: Fn(T) -> Result<(), E>,
{
    let it = it.into_iter();
    if fail_fast {
        it.fold(Ok(()), |r, v| {
            let do_f = &do_f;
            r.and_then(move |_| do_f(v))
        })
    } else {
        it
            .map(do_f)
            .fold(Ok(()), Result::and)
    }
}

fn do_unmount<S: AsRef<str>>(config: &Config, name: S) -> Result<(), MountError> {
    use util::CommandExt;
    let name = name.as_ref();
    let info = if let Some(v) = config.get_mount(name) {
        v
    } else {
        return Err(MountError::UnknownMount(name.to_owned()));
    };
    Command::new("sudo")
        .arg("umount")
        .arg(info.local_path())
        .status_checked()
        .map_err(|e| MountError::Command(name.to_owned(), e))
        .map(|_| ())
}

fn list_mounts(config: &Config, verbose: bool) -> Result<(), Infallible> {
    use colored::{
        Colorize,
        Color,
    };
    config.mounts().for_each(|(name, mount)| {
        if verbose {
            let uri = mount.uri();
            let uri: &str = uri.as_ref();
            let local_path = format!("{}", mount.local_path().display());
            let local_path: &str = local_path.as_ref();
            println!("{}: {} {} {}", name.bold().color(Color::Cyan), local_path.color(Color::Blue), "->".dimmed(), uri.color(Color::Blue));
        } else {
            println!("{}", name);
        }
    });
    Ok(())
}

impl RunnableConfig for Options {
    type Error = Box<dyn Error>;
    fn run(&self) -> Result<(), Self::Error> {
        let config = self.config()?;
        debug!("config: {:?}", &config);
        match &self.command {
            SubCommand::Mount { names } => do_all(names, |name| do_mount(&config, name), false)?,
            SubCommand::Umount { names } => do_all(names, |name| do_unmount(&config, name), false)?,
            &SubCommand::List { verbose } => list_mounts(&config, verbose)?,
        }
        Ok(())
    }
}

const EXIT_FAILURE: i32 = 1;

fn main() {
    use clap::Parser;
    logging::init();
    let options = Options::parse();
    if let Err(e) = options.run() {
        error!("{}", &e);
        process::exit(EXIT_FAILURE);
    }
}
