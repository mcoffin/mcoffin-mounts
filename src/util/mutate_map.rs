pub trait ExtMutateMap<T>: Sized {
    fn mutate_map<F>(self, f: F) -> Self
    where
        F: FnMut(&mut T);
}

impl<T: Sized> ExtMutateMap<T> for Option<T> {
    #[inline]
    fn mutate_map<F>(mut self, mut f: F) -> Self
    where
        F: FnMut(&mut T),
    {
        if let Some(v) = self.as_mut() {
            f(v);
        }
        self
    }
}
