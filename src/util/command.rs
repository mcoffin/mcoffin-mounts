use std::{
    io,
    process::{
        Command,
        ExitStatus,
    },
};

#[derive(Debug, thiserror::Error)]
pub enum CommandError {
    #[error("Error running command: {0}")]
    Io(#[from] io::Error),
    #[error("Command exited with failure status: {0}")]
    Status(ExitStatus),
}

pub fn check_status(status: ExitStatus) -> Result<ExitStatus, CommandError> {
    if status.success() {
        Ok(status)
    } else {
        Err(CommandError::Status(status))
    }
}

pub trait CommandExt {
    fn status_checked(&mut self) -> Result<ExitStatus, CommandError>;
}

impl CommandExt for Command {
    fn status_checked(&mut self) -> Result<ExitStatus, CommandError> {
        self.status()
            .map_err(CommandError::from)
            .and_then(check_status)
    }
}
